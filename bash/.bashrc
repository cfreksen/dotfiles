#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

BOLD="\[$(tput bold)\]"
ITALICS="\[$(tput sitm)\]"
RESET="\[$(tput sgr0)\]"
PS1="${BOLD}[\u@\h ${ITALICS}\W${RESET}${BOLD}]\$${RESET} "

export HISTSIZE=-1
export HISTFILESIZE=-1
export HISTCONTROL=ignorespace:erasedups

export PATH="$HOME/.local/bin:$PATH"

alias sauth='sadmin auth -u casper && ssh-add -t 1d'

# Print terrastream images as they are built
terrastream-ci ()
{
    sadmin list-images -e '{time} {labels[GIT_BRANCH]} docker:sadmin.scalgo.com/{image}@{hash}' -fn0 -i terrastream | while read time branch hash; do
        printf "\a[%s] %s: %s\n" "`date -d@$time +%H:%M:%S`" "$branch" "$hash";
    done
}

# Print newest terrastream images
terrastream-builds ()
{
    sadmin list-images -n10 -iterrastream -e '{green}{bold}{rel_time}{reset} {red}{bold}{tag}{reset} {image} commit {labels[GIT_COMMIT_FULL]} (tip of {labels[GIT_BRANCH]}) {green}docker:sadmin.scalgo.com/{image}@{hash}{reset}' "$@"
}


bind 'set enable-bracketed-paste on'
bind 'set completion-ignore-case on'

if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
  echo perhaps run startx
fi
