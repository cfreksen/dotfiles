This is just a small emacs configuration with a few settings one can
use while setting up a system.

I suggest I use https://gitlab.com/cfreksen/dot-emacs after the
initial setup.
