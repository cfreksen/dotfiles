;; General editor behaviour
(show-paren-mode t)
(fset 'yes-or-no-p 'y-or-n-p)
(setq visible-bell 1)
(transient-mark-mode t)
(global-font-lock-mode t)
(setq-default show-trailing-whitespace t)
(put 'downcase-region 'disabled nil)
(setq mouse-yank-at-point t)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq tab-stop-list '(0 4 8))
(setq save-interprogram-paste-before-kill t)

;; Ido
(ido-mode t)
(setq ido-auto-merge-work-directories-length -1)

;; Backups
(let ((bdir (concat user-emacs-directory "backups")))
  (unless (file-exists-p bdir)
    (make-directory bdir))
  (setq backup-directory-alist `(("." . ,bdir)))
  (setq make-backup-files t
        backup-by-copying t
        version-control t
        delete-old-versions t
        delete-by-moving-to-trash t
        kept-old-versions 6
        kept-new-versions 9
        auto-save-default t
        auto-save-timeout 30
        auto-save-interval 300))

;; Dired
(setq dired-listing-switches
      "-alh --time-style=long-iso --group-directories-first")

;; UTF-8
(set-language-environment "UTF-8")
(setq locale-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;; GUI
(setq inhibit-startup-message t
      initial-scratch-message "; æøå\n")
(line-number-mode 1)
(column-number-mode 1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq frame-title-format "Emacs: %b")
