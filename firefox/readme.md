1. In firefox, visit about:support
2. Look for the "Profile Directory", it should be something like
   `/home/cfreksen/.mozilla/firefox/ii16zyj2.default-release`
3. Create a subfolder in that directory and name it `chrome`
4. Copy/symlink `userChrome.css` into `chrome`
5. In firefox, visit about:config
6. Set `toolkit.legacyUserProfileCustomizations.stylesheets` to true
